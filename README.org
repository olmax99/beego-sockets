#+TITLE: beego-sockets
#+SETUPFILE: ~/s3sync/org/conf/setup.config
#+FILETAGS: :golang:sockets:beego:web:long:polling

[中文文档](./README_ZH.md)

This sample is about using long polling and WebSocket to build a web-based chat room based on beego.

- [Documentation](http://beego.me/docs/examples/chat.md)

* Installation
#+begin_src bash
$ cd $GOPATH/src/samples/WebIm
$ go get github.com/gorilla/websocket
$ go get github.com/beego/i18n
$ bee run
#+end_src

* Usage
- enter chat room from 
#+begin_src bash
http://127.0.0.1:8080 
#+end_src

* FAQ
** 1. How are the =controller/chatroom.go= channels loaded?
- =main.go= > imports =routers= > imports =controllers=
  #+begin_src go
// chatroom.go

// all controllers in 'package controllers'
// all init() are being run upon import
104 func init() {
105 	go chatroom() // This function handles all incoming chan messages.
106 }

// also global vars are imported even before init() is executed upon import
var (
	// Channel for new join users.
	subscribe = make(chan Subscriber, 10)
	// Channel for exit users.
	unsubscribe = make(chan string, 10)
	// Send events here to publish them.
	publish = make(chan models.Event, 10)
	// Long polling waiting list.
	waitingList = list.New()
	subscribers = list.New()
)
  #+end_src

** 2. How is the front-end polling for new messages?
- There are two main actions with long-polling for events.
- A third essential component is the [[file:controllers/chatroom.go::func chatroom() {][controller/chatroom.go]] 

*** 1. Create a new Event
- This starts with the [[file:views/longpolling.html::<button id="sendbtn" type="button" class="btn btn-default">{{i18n .Lang "Send"}}</button>][views/longpolling Send Button]], which executes /JQuery/:
#+begin_src js
// longpolling.js
$(document).ready(function () {
    var postConecnt = function () {
    ..
        $.post("/lp/post", {
            uname: uname,
            content: content
        });
    ..
    $('#sendbtn').click(function () {
        postConecnt();

// controllers/logpolling.go
func (this *LongPollingController) Post() {
..
publish <- newEvent(models.EVENT_MESSAGE, uname, content)
#+end_src
- The click action triggers the controller's [[file:controllers/longpolling.go::func (this *LongPollingController) Post() {][Post()]] at =/lp/post=, which is
  creating a new Event message
- _NOTE_: In a pure notification scenario this could be replaced by a simple
  endpoint call from within the controller posting a /notification message/ just
  before redirecting back to the starting point.

*** 2. Polling for new Events
- Starting point is [[file:views/longpolling.html::<div class="container">][views/longpolling.html <ul> id=chatbox]]
- In an [[file:static/js/longpolling.js::setInterval(fetch, 3000);][interval of 3 sec]], Js [[file:static/js/longpolling.js::var fetch = function () {][fetch()]] is calling JSON endpoint =/lp/fetch?lastReceived=0=
  (either '0' or a timestamp)
- The controller's [[file:controllers/longpolling.go::func (this *LongPollingController) Fetch() {][Fetch()]] method yields an event list object from a /doubly linked
  list/, which is fully handled in [[file:~/.gvm/pkgsets/go1.17.3/global/pkg/mod/github.com/beego/samples@v0.0.0-20180420090448-1c696ee4905b/WebIM/models/archive.go::type Event struct {][models.archive]] (as =ServeJSON()=). '0' or timestamp is used to
  evaluate the new Event against the last.
- The [[file:models/archive.go::const archiveSize = 20][archiveSize]] determines the number of visible Events.
- And the full Event (linked-)list can be 

*** 3. Chatroom
- This is a special controller as it does not handle any router endpoints.
- _NOTE_: The 'unsubscribe chan' seems irrelevant for long-polling.
- For every /publish/ event, the archive is (re-)initialized meaning that the
  archive length is (re-)set to 20.
- The new /publish/ event, which caused the renewal of the archive is pushed to
  the back, while the Front is removed
#+begin_src go
func NewArchive(event Event) {
if archive.Len() >= archiveSize {
  	archive.Remove(archive.Front())
}
archive.PushBack(event)
#+end_src

** 3. How does long-polling achieve the persistent message board?

** 4. How can messages being made visible only per session?
- In case of long-polling only for notifications, there is no persistent
  messaging board needed.
- Rather every message could be delivered, and gradually disappear.
